<h1>Dr. Semmelweis and the Discovery of Handwashing.</h1>
<h4>Project Description</h4>
<p>In 1847, the Hungarian physician Ignaz Semmelweis makes a breakthough discovery: He discovers handwashing. Contaminated hands was a major cause of childbed fever and by enforcing handwashing at his hospital he saved hundreds of lives.</p>
<h4>Project Tasks</h4>
<ul class="dc-task-list">
<li class="dc-task-list__item"><span class="dc-task-list__item-number">1&nbsp;</span><span class="dc-task-list__item-name">Meet Dr. Ignaz Semmelweis</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">2&nbsp;</span><span class="dc-task-list__item-name">The alarming number of deaths</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">3&nbsp;</span><span class="dc-task-list__item-name">Death at the clinics</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">4&nbsp;</span><span class="dc-task-list__item-name">The handwashing begins</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">5&nbsp;</span><span class="dc-task-list__item-name">he effect of handwashing</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">6&nbsp;</span><span class="dc-task-list__item-name">The effect of handwashing highlighted</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">7&nbsp;</span><span class="dc-task-list__item-name">More handwashing, fewer deaths?</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">8&nbsp;</span><span class="dc-task-list__item-name">A Bootstrap analysis of Semmelweis handwashing data</span></li>
<li class="dc-task-list__item"><span class="dc-task-list__item-number">9&nbsp;</span><span class="dc-task-list__item-name">The fate of Dr. Semmelweis</span></li>
</ul>
<h2>Acknowledgement:</h2>
<p>DataCamp's&nbsp;<a href="https://www.datacamp.com/projects/20" rel="nofollow">Dr. Semmelweis and the Discovery of Handwashing</a>&nbsp;Project.</p>